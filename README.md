Avi.OpenMod.DynamicCommandSource package
----------------------------------------

An OpenMod mini-framework that allows dynamically adding/removing commands at runtime.

#### Usage:

1. Install Nuget dependency `Avi.OpenMod.DynamicCommandSource`.
2. Use `IDynamicCommandsManager` service in your `OpenModComponent` to register/unregister commands at runtime.

You can use `IDynamicCommandsManager.RegisterFromConfigAsync` extension to register commands from a `commands.yaml` file (an EmbeddedResource file in root of your project similar to `config.yaml` or `translations.yaml`). The file should follow this formatting:

```yaml
command1:
  type_name: Avi.OpenMod.SomePlugin.Command1Type
  enabled: true
  names: # an array of names, minimum 1 element
    - "c1"
    - "cmd1"
  # syntax override is optional, but helps a lot if you translate commands to other language
  syntax_override: <add/+/remove/-/list/?/ui>

command2:
  type_name: Avi.OpenMod.SomePlugin.Command2Type
  enabled: true
  names:
    - "list"
    - "?"
```

License
=======

    Copyright 2021 aviadmini

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.