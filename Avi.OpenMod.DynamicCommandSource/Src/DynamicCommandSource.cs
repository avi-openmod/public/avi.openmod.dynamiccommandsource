using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

using Microsoft.Extensions.Logging;

using OpenMod.API;
using OpenMod.API.Commands;
using OpenMod.Core.Commands;

namespace Avi.OpenMod.DynamicCommandSource {

    // ReSharper disable once ClassNeverInstantiated.Global
    internal class DynamicCommandSource : ICommandSource {

        private readonly IOpenModComponent _component;
        private readonly ICommandDataStore _commandDataStore;

        private readonly List<DynamicComponentBoundCommandRegistration> _commandRegistrations = new();

        public Task<IReadOnlyCollection<ICommandRegistration>> GetCommandsAsync()
            => Task.FromResult(_commandRegistrations as IReadOnlyCollection<ICommandRegistration>);

        private readonly ILogger<DynamicCommandSource> _logger;

        public DynamicCommandSource(IOpenModComponent component, ILogger<DynamicCommandSource> logger, ICommandDataStore commandDataStore) {
            _component = component;
            _logger = logger;
            _commandDataStore = commandDataStore;
        }

        public Task<string> AddTypeAsync(Type type, ICollection<string> aliases, string? syntaxOverride)
            => AddRegistrationAsync(new DynamicComponentBoundCommandRegistration(_component, type, aliases, syntaxOverride));

        public Task<string> AddMethodAsync(MethodInfo info, ICollection<string> aliases, string? syntaxOverride)
            => AddRegistrationAsync(new DynamicComponentBoundCommandRegistration(_component, info, aliases, syntaxOverride));

        private async Task<string> AddRegistrationAsync(DynamicComponentBoundCommandRegistration registration) {

            // add registration
            _commandRegistrations.Add(registration);

            // override stuff in commands data store
            RegisteredCommandsData commands = await _commandDataStore.GetRegisteredCommandsAsync();
            if (commands.Commands != null) {

                int idx = commands.Commands.FindIndex(d => d.Id?.Equals(registration.Id, StringComparison.OrdinalIgnoreCase) ?? false);
                if (idx != -1) {

                    RegisteredCommandData toChange = commands.Commands[idx];
                    bool changed = false;

                    // force override parent id, name, aliases, enabled and syntax

                    if (toChange.ParentId != registration.ParentId) {
                        toChange.ParentId = registration.ParentId;
                        changed = true;
                    }

                    if (!registration.Name.Equals(toChange.Name, StringComparison.OrdinalIgnoreCase)) {
                        toChange.Name = registration.Name;
                        changed = true;
                    }

                    if (toChange.Aliases != null &&
                        registration.Aliases != null &&
                        registration.Aliases.Any(a => !toChange.Aliases.Contains(a, StringComparer.OrdinalIgnoreCase))
                    ) {
                        toChange.Aliases = registration.Aliases?.ToList();
                        changed = true;
                    }

                    if (toChange.Enabled != registration.IsEnabled) {
                        toChange.Enabled = registration.IsEnabled;
                        changed = true;
                    }

                    //TODO add syntax 
                    // if (toChange.Syntax != registration.Syntax) {
                    //     toChange.Syntax = registration.Syntax;
                    //     changed = true;
                    // }

                    if (changed) {
                        _logger.LogInformation("Overriding dynamic command '{Id}' RegisteredCommandData", registration.Id);
                        await _commandDataStore.SetRegisteredCommandsAsync(commands);
                    }

                }

            }

            _logger.LogInformation("Registered dynamic command '{Id}' with name '{Name}', aliases: {Aliases}",
                registration.Id, registration.Name, registration.Aliases);

            return registration.Id;
        }

        public bool Remove(string registrationId) {

            DynamicComponentBoundCommandRegistration? cmd =
                _commandRegistrations.FirstOrDefault(it => registrationId == it.Id);
            if (cmd == null) {
                return false;
            }

            if (_commandRegistrations.Remove(cmd)) {
                _logger.LogInformation("Removed dynamic command for '{Name}'", cmd.Name);
            }

            return true;
        }

    }

}