using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

using Autofac;

using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

using OpenMod.API;
using OpenMod.API.Ioc;
using OpenMod.Core.Commands;

namespace Avi.OpenMod.DynamicCommandSource {

    [ServiceImplementation(Lifetime = ServiceLifetime.Scoped)]
    // ReSharper disable once UnusedType.Global
    internal class DynamicCommandsManager : IDynamicCommandsManager, IDisposable {

        private readonly Lazy<IOpenModComponent> _lazyComponent;
        private readonly IOptions<CommandStoreOptions> _storeOptions;

        public IOpenModComponent Component => _lazyComponent.Value;

        private DynamicCommandSource? _commandSource;

        public DynamicCommandsManager(
            Lazy<IOpenModComponent> lazyComponent,
            IOptions<CommandStoreOptions> storeOptions
        ) {
            _lazyComponent = lazyComponent;
            _storeOptions = storeOptions;
        }

        public Task<string> RegisterTypeAsync(Type type, ICollection<string> aliases, string? syntaxOverride = null)
            => AddAsync(type, aliases, syntaxOverride);

        public Task<string> RegisterMethodAsync(MethodInfo info, ICollection<string> aliases, string? syntaxOverride = null)
            => AddAsync(info, aliases, syntaxOverride);

        public bool Unregister(string registrationId)
            => _commandSource?.Remove(registrationId) ?? false;

        private async Task<string> AddAsync(MemberInfo memberInfo, ICollection<string> aliases, string? syntaxOverride) {

            _commandSource ??= Component.LifetimeScope.Resolve<DynamicCommandSource>();

            string registrationId = memberInfo switch {
                Type type => await _commandSource.AddTypeAsync(type, aliases, syntaxOverride),
                MethodInfo info => await _commandSource.AddMethodAsync(info, aliases, syntaxOverride),
                _ => throw new Exception("Cannot dynamically register command that is not type or method based")
            };

            // command permissions are registered automatically

            _storeOptions.Value.RemoveCommandSource(_commandSource);
            _storeOptions.Value.AddCommandSource(_commandSource);

            return registrationId;
        }

        public void Dispose() {

            if (_commandSource == null) {
                return;
            }

            _storeOptions.Value.RemoveCommandSource(_commandSource);
        }

    }

}