using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

using Microsoft.Extensions.DependencyInjection;

using OpenMod.API;
using OpenMod.API.Commands;
using OpenMod.API.Permissions;
using OpenMod.API.Prioritization;
using OpenMod.Core.Commands;
using OpenMod.Core.Permissions;

namespace Avi.OpenMod.DynamicCommandSource {

    /// <summary>
    /// Represents a command registered dynamically (at runtime) 
    /// </summary>
    internal class DynamicComponentBoundCommandRegistration : ICommandRegistration {

        /// <summary>
        /// Command type if the command is type-based, otherwise null
        /// </summary>
        public Type? CommandType { get; }

        /// <summary>
        /// Command method if the command is method-based, otherwise null
        /// </summary>
        public MethodInfo? CommandMethod { get; }

        private ICollection<Type> _commandActorTypes = null!;

        /// <summary>
        /// Creates a command registration for a type
        /// </summary>
        /// <param name="component">the component</param>
        /// <param name="type">command type</param>
        /// <param name="names">command names</param>
        /// <param name="syntaxOverride">syntax override</param>
        public DynamicComponentBoundCommandRegistration(
            IOpenModComponent component,
            Type type,
            ICollection<string> names,
            string? syntaxOverride
        ) {
            Component = component;
            Name = names.First();
            Aliases = names.Count == 1 ? null : names.Skip(1).ToArray();
            CommandType = type;
            ReadAttributes(CommandType, syntaxOverride);
        }

        /// <summary>
        /// Creates a command registration for a method
        /// </summary>
        /// <param name="component">the component</param>
        /// <param name="info">command method</param>
        /// <param name="names">command names</param>
        /// <param name="syntaxOverride">syntax override</param>
        public DynamicComponentBoundCommandRegistration(
            IOpenModComponent component,
            MethodInfo info,
            ICollection<string> names,
            string? syntaxOverride
        ) {
            Component = component;
            Name = names.First();
            Aliases = names.Count == 1 ? null : names.Skip(1).ToArray();
            CommandMethod = info;
            ReadAttributes(CommandMethod, syntaxOverride);
        }

        private static string GetIdSlug()
            => $"-{Guid.NewGuid().ToString().Replace("-", "")[..6]}";

        private void ReadAttributes(MemberInfo memberInfo, string? syntaxOverride) {

            Id = (CommandType != null
                     ? CommandType!.FullName!
                     : $"{CommandMethod!.DeclaringType?.FullName ?? "DynamicMethod"}.{CommandMethod!.Name}")
                 + GetIdSlug();

            Priority? priority = null;

            Priority = priority ?? memberInfo.GetCustomAttribute<PriorityAttribute>()?.Priority ?? Priority.Normal;
            Description ??= memberInfo.GetCustomAttribute<CommandDescriptionAttribute>()?.Description;
            Syntax ??= syntaxOverride ?? memberInfo.GetCustomAttribute<CommandSyntaxAttribute>()?.Syntax;
            PermissionRegistrations =
                memberInfo.GetCustomAttributes<RegisterCommandPermissionAttribute>(true)
                    .Select(d => new PermissionRegistration {
                        DefaultGrant = d.DefaultGrant,
                        Description = d.Description,
                        Owner = Component,
                        Permission = d.Permission
                    }).ToList();

            _commandActorTypes = memberInfo.GetCustomAttributes<CommandActorAttribute>().Select(d => d.ActorType).ToList();

            var parentAttribute = memberInfo.GetCustomAttribute<CommandParentAttribute>();
            if (parentAttribute != null) {
                ParentId = parentAttribute.CommandType != null
                    ? parentAttribute.CommandType.FullName!
                    : $"{parentAttribute.DeclaringType!.FullName}.{parentAttribute.MethodName}";
            }

        }

        /// <inheritdoc />
        public bool SupportsActor(ICommandActor actor)
            => !_commandActorTypes.Any() || _commandActorTypes.Any(d => d.IsInstanceOfType(actor));

        /// <inheritdoc />
        public ICommand Instantiate(IServiceProvider serviceProvider) => CommandType != null
            ? (ICommand) ActivatorUtilities.CreateInstance(serviceProvider, CommandType)
            : new MethodCommandWrapper(CommandMethod!, serviceProvider);

        /// <inheritdoc />
        public IOpenModComponent Component { get; }

        /// <inheritdoc />
        public string Id { get; private set; } = null!;

        /// <inheritdoc />
        public string Name { get; }

        /// <inheritdoc />
        public IReadOnlyCollection<string>? Aliases { get; }

        /// <inheritdoc />
        public IReadOnlyCollection<IPermissionRegistration>? PermissionRegistrations { get; private set; }

        /// <inheritdoc />
        public string? Description { get; private set; }

        /// <inheritdoc />
        public string? Syntax { get; private set; }

        /// <inheritdoc />
        public Priority Priority { get; private set; }

        /// <inheritdoc />
        public string? ParentId { get; private set; }

        /// <inheritdoc />
        public bool IsEnabled => Component.IsComponentAlive;

    }

}