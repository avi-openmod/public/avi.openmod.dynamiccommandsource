using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Autofac;

using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

using OpenMod.API.Commands;

// ReSharper disable UnusedMember.Global

namespace Avi.OpenMod.DynamicCommandSource {

#pragma warning disable 1591
    // ReSharper disable once UnusedType.Global
    public static class DynamicCommandsManagerExtensions {

#pragma warning restore 1591

        /// <summary>
        /// Same as <see cref="IDynamicCommandsManager.RegisterTypeAsync"/> but with generic T
        /// </summary>
        public static Task RegisterClassAsync<T>(
            this IDynamicCommandsManager commander, ICollection<string> aliases, string? syntaxOverride = null
        ) where T : class, ICommand
            => commander.RegisterTypeAsync(typeof(T), aliases, syntaxOverride);

        /// <summary>
        /// Register all commands from commands.yaml
        /// </summary>
        public static async Task RegisterFromConfigAsync(this IDynamicCommandsManager manager) {

            IConfigurationRoot cfg = new ConfigurationBuilder()
                .SetBasePath(manager.Component.WorkingDirectory)
                .AddYamlFile("commands.yaml", false)
                .Build();

            Type loggerType = typeof(ILogger<>).MakeGenericType(manager.Component.GetType());
            var logger = (ILogger) manager.Component.LifetimeScope.Resolve(loggerType);

            foreach (IConfigurationSection command in cfg.GetChildren()) {

                // check if enabled
                bool enabled = command.GetValue<bool>("enabled");
                if (!enabled) {
                    logger.LogInformation("Command binding for '{Name}' not enabled ('enabled: true')", command.Key);
                    continue;
                }

                // get type name
                string typeName = command.GetValue<string>("type_name");
                if (typeName == null) {
                    logger.LogWarning("'type_name' not set for command registration '{Key}'", command.Key);
                    continue;
                }

                // get type
                Type type = manager.Component.GetType().Assembly.GetType(typeName);
                if (type == null) {
                    logger.LogWarning("Cannot find type '{Type}' for command registration '{Key}'", typeName, command.Key);
                    continue;
                }

                // check names
                IConfigurationSection namesSection = command.GetSection("names");
                if (!namesSection.Exists()) {
                    logger.LogWarning("Command binding for '{Name}' doesn't contain 'names' array", command.Key);
                    continue;
                }

                // get names
                string[] cmdNames = namesSection.Get<string[]>();
                if (!cmdNames.Any()) {
                    logger.LogWarning("Command binding for '{Name}' has empty 'names' array", command.Key);
                    continue;
                }

                if (cmdNames.All(string.IsNullOrWhiteSpace)) {
                    logger.LogWarning("Command binding for '{Name}' only contains empty/invalid name/aliases", command.Key);
                    continue;
                }

                string? syntaxOverride = command.GetValue<string?>("syntax_override");

                await manager.RegisterTypeAsync(type, cmdNames.Where(x => !string.IsNullOrWhiteSpace(x)).ToArray(), syntaxOverride);

            }

        }

    }

}