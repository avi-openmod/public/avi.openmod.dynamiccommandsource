using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

using OpenMod.API;
using OpenMod.API.Ioc;

// ReSharper disable UnusedMember.Global

namespace Avi.OpenMod.DynamicCommandSource {

    /// <summary>
    /// This service allows registering/unregistering commands at runtime
    /// </summary>
    [Service]
    public interface IDynamicCommandsManager {

        /// <summary>
        /// Component for which dynamic commands are registered (by this service instance)
        /// </summary>
        IOpenModComponent Component { get; }

        /// <summary>
        /// Register type-based command
        /// </summary>
        /// <param name="type">command type</param>
        /// <param name="aliases">command names</param>
        /// <param name="syntaxOverride">command syntax override</param>
        /// <returns>Registration ID</returns>
        Task<string> RegisterTypeAsync(Type type, ICollection<string> aliases, string? syntaxOverride = null);

        /// <summary>
        /// Register method-based command
        /// </summary>
        /// <param name="info">command method</param>
        /// <param name="aliases">command names</param>
        /// <param name="syntaxOverride">command syntax override</param>
        /// <returns>Registration ID</returns>
        Task<string> RegisterMethodAsync(MethodInfo info, ICollection<string> aliases, string? syntaxOverride = null);

        /// <summary>
        /// Unregister command by registration ID
        /// </summary>
        /// <param name="registrationId">registration ID</param>
        /// <returns>true if registration was removed</returns>
        bool Unregister(string registrationId);

    }

}