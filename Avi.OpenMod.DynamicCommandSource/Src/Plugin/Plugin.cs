using System;

using OpenMod.API.Plugins;
using OpenMod.Core.Plugins;

[assembly: PluginMetadata(
    "Avi.OpenMod.DynamicCommandSource",
    Author = "aviadmini",
    DisplayName = "Avi Dynamic Command Source",
    Website = "https://avi.pw/discord"
)]

namespace Avi.OpenMod.DynamicCommandSource {

    // ReSharper disable once UnusedType.Global
    internal class Plugin : OpenModUniversalPlugin {

        public Plugin(IServiceProvider serviceProvider) : base(serviceProvider) { }

    }

}