using Autofac;

using OpenMod.API.Ioc;

namespace Avi.OpenMod.DynamicCommandSource {

    // ReSharper disable once UnusedType.Global
    internal class ContainerConfigurator : IContainerConfigurator {

        public void ConfigureContainer(IOpenModServiceConfigurationContext openModStartupContext, ContainerBuilder containerBuilder) {
            containerBuilder.RegisterType<DynamicCommandSource>().AsSelf();
        }

    }

}